Drupal Professional theme Clean Theme
--------------------------------------------------------------------------
Drupal Professional theme Clean Theme is a clean theme,
multi-column Drupal 7 theme with many block 
regions and a slideshow ready for use.

Clean design
Support custom logo
Fixed width 1000px
Multiple columns - 1, 2, or 3
Multi-level drop-down menus
Many block regions
Top four region
Bottom four region
Nivo slideshow

Menu:
--------------------------------------
Install Superfish module

Drupal compatibility:
--------------------------------------
Install Nivo slider

Drupal compatibility:
--------------------------------------
This theme is compatible with Drupal 7.x.x

--------------------------------------
Developed by irshadfreelancer.com
